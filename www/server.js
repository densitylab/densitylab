var http = require("http");
var url = require("url");
var pot1;
var pot2;
var lavariable2;

function peticionServidor(req, resp) {
	console.log("Peticion!!")
	resp.writeHead(200,{"Content-Type" : "text/html"});
	resp.write("<h1>Valor de los Sensores</h1>");
	resp.write("<p>Sensor1: " + pot1 + "<br>Sensor2: " + pot2 + "<br>" + lavariable2 + "</p>");
	var value = pot1;
	console.log("el pot es:  = "+value);
	//resp.write();
	resp.end();
}

var SerialPort = require("/usr/lib/node_modules/serialport").SerialPort;
var serialPort = new SerialPort("/dev/ttySP1", {baudrate: 115200});

var io = require('/usr/lib/node_modules/socket.io').listen(8000);

io.sockets.on('connection', function (socket) {
	// If socket.io receives message from the client browser then 
    // this call back will be executed.
    socket.on('message', function (msg) {
    	console.log(msg);
    });
    // If a web browser disconnects from Socket.IO then this callback is called.
    socket.on('disconnect', function () {
    	console.log('disconnected');
    });
});

serialPort.on("open", function () {
		var cadena;
    console.log('open');        
    serialPort.on("data", function (data) {
    	if (data.length <= 8) {
    		if (data.length == 8) {
        	lavariable2 = data + '';
      	} else {
      		cadena = lavariable2.concat(data);               
        	pot1 = cadena.substring(2,4);
        	pot2 = cadena.substring(7,9);
        	io.sockets.emit("sensor1", pot1);
		    	io.sockets.emit("sensor2", pot2);
      	}                              
      } else {
      	cadena = data + '';                
        pot1 = cadena.substring(2,4);
        pot2 = cadena.substring(7,9); 
        io.sockets.emit("sensor1", pot1);
		    io.sockets.emit("sensor2", pot2);
      }
      console.log(cadena.length+". "+" data ="+ cadena);
    });            
});

http.createServer(peticionServidor).listen(8888);
console.log("servidor Creado");

/*
var cleanData = ''; // this stores the clean data
var readData = '';  // this stores the buffer
sp.on('data', function (data) { // call back when data is received
    readData += data.toString(); // append data to buffer
    // if the letters 'A' and 'B' are found on the buffer then isolate what's in the middle
    // as clean data. Then clear the buffer. 
    if (readData.indexOf('B') >= 0 && readData.indexOf('A') >= 0) {
        cleanData = readData.substring(readData.indexOf('A') + 1, readData.indexOf('B'));
        readData = '';
        io.sockets.emit('message', cleanData);
	}
});
*/
