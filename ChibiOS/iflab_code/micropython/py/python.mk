PYTHON_ASM      =${PY_SRC}/nlrthumb.s


PYTHON_SRC      = ${PY_SRC}/nlrsetjmp.c \
                  ${PY_SRC}/malloc.c \
                  ${PY_SRC}/gc.c \
                  ${PY_SRC}/qstr.c \
                  ${PY_SRC}/vstr.c \
                  ${PY_SRC}/unicode.c \
                  ${PY_SRC}/mpz.c \
                  ${PY_SRC}/lexer.c \
                  ${PY_SRC}/lexerstr.c \
                  ${PY_SRC}/lexerunix.c \
                  ${PY_SRC}/parse.c \
                  ${PY_SRC}/parsehelper.c \
                  ${PY_SRC}/scope.c \
                  ${PY_SRC}/compile.c \
                  ${PY_SRC}/emitcommon.c \
                  ${PY_SRC}/emitpass1.c \
                  ${PY_SRC}/emitcpy.c \
                  ${PY_SRC}/emitbc.c \
                  ${PY_SRC}/asmarm.c \
                  ${PY_SRC}/emitnative.c \
                  ${PY_SRC}/formatfloat.c \
                  ${PY_SRC}/parsenumbase.c \
                  ${PY_SRC}/parsenum.c \
                  ${PY_SRC}/emitglue.c \
                  ${PY_SRC}/runtime.c \
                  ${PY_SRC}/nativeglue.c \
                  ${PY_SRC}/stackctrl.c \
                  ${PY_SRC}/argcheck.c \
                  ${PY_SRC}/map.c \
                  ${PY_SRC}/obj.c \
                  ${PY_SRC}/objarray.c \
                  ${PY_SRC}/objbool.c \
                  ${PY_SRC}/objboundmeth.c \
                  ${PY_SRC}/objcell.c \
                  ${PY_SRC}/objclosure.c \
                  ${PY_SRC}/objcomplex.c \
                  ${PY_SRC}/objdict.c \
                  ${PY_SRC}/objenumerate.c \
                  ${PY_SRC}/objexcept.c \
                  ${PY_SRC}/objfilter.c \
                  ${PY_SRC}/objfloat.c \
                  ${PY_SRC}/objfun.c \
                  ${PY_SRC}/objgenerator.c \
                  ${PY_SRC}/objgetitemiter.c \
                  ${PY_SRC}/objint.c \
                  ${PY_SRC}/objint_longlong.c \
                  ${PY_SRC}/objint_mpz.c \
                  ${PY_SRC}/objlist.c \
                  ${PY_SRC}/objmap.c \
                  ${PY_SRC}/objmodule.c \
                  ${PY_SRC}/objobject.c \
                  ${PY_SRC}/objproperty.c \
                  ${PY_SRC}/objnone.c \
                  ${PY_SRC}/objnamedtuple.c \
                  ${PY_SRC}/objrange.c \
                  ${PY_SRC}/objreversed.c \
                  ${PY_SRC}/objset.c \
                  ${PY_SRC}/objslice.c \
                  ${PY_SRC}/objstr.c \
                  ${PY_SRC}/objstrunicode.c \
                  ${PY_SRC}/objstringio.c \
                  ${PY_SRC}/objtuple.c \
                  ${PY_SRC}/objtype.c \
                  ${PY_SRC}/objzip.c \
                  ${PY_SRC}/opmethods.c \
                  ${PY_SRC}/sequence.c \
                  ${PY_SRC}/stream.c \
                  ${PY_SRC}/binary.c \
                  ${PY_SRC}/builtin.c \
                  ${PY_SRC}/builtinimport.c \
                  ${PY_SRC}/builtinevex.c \
                  ${PY_SRC}/builtintables.c \
                  ${PY_SRC}/modarray.c \
                  ${PY_SRC}/modcollections.c \
                  ${PY_SRC}/modgc.c \
                  ${PY_SRC}/modio.c \
                  ${PY_SRC}/modmath.c \
                  ${PY_SRC}/modcmath.c \
                  ${PY_SRC}/modmicropython.c \
                  ${PY_SRC}/modstruct.c \
                  ${PY_SRC}/modsys.c \
                  ${PY_SRC}/vm.c \
                  ${PY_SRC}/bc.c \
                  ${PY_SRC}/showbc.c \
                  ${PY_SRC}/repl.c \
                  ${PY_SRC}/smallint.c \
                  ${PY_SRC}/pfenv.c \
                  ${PY_SRC}/pfenv_printf.c \
                  ${PY_SRC}/moductypes.c  \
                  ${PY_SRC}/modujson.c    \
                  ${PY_SRC}/modzlibd.c


                  

# Required include directories
PYTHON_INC = ${PY_SRC}
