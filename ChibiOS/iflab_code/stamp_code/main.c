#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"
#include "atmel_adc.h"
#include "atmel_pwm.h"

#define PWM_FREQUENCY               1000
#define MAX_DUTY_CYCLE              50
#define MIN_DUTY_CYCLE              0


int16_t adc_values1[1] = { 0 } ;
int16_t adc_values2[1] = { 0 } ;

volatile unsigned char conversionDone = 0 ;
volatile unsigned char pot = 0 ;

static WORKING_AREA(waThread1, 128);
static msg_t Thread1(void *arg) {
  (void)arg;
  while (TRUE) {
    palClearPad(IOPORT2, PIOB_LED2);
    chThdSleepMilliseconds(500);
    palSetPad(IOPORT2, PIOB_LED2);
    chThdSleepMilliseconds(500);
  }
  return(0);
}

static void serve_adc_interrupt(void){
	
	uint32_t status;
	status = ADC_GetStatus(ADC);

	if ( (status & ADC_ISR_RXBUFF) == ADC_ISR_RXBUFF )
	{
		conversionDone = 1;
		if (!pot) ADC_ReadBuffer( ADC, adc_values1, 1 ) ;  //Lee solo un valor
		else ADC_ReadBuffer( ADC, adc_values2, 1 ) ;
	}	
}


CH_IRQ_HANDLER(ADC_IRQHandler)
{
    CH_IRQ_PROLOGUE();
    serve_adc_interrupt();
    CH_IRQ_EPILOGUE();
}

/*
 * Application entry point.
 */
int main(void) {
	
	int status ;

	halInit();
	chSysInit();

	sdStart(&SD1, NULL);  /* Activates the serial driver 2 */

	/* ADC configuration*/
	ADC_Initialize( ADC);
	ADC_cfgFrequency( ADC, 15, 11 ); //
	ADC_check( ADC, 48000000 ); // Board Clock 48000000

	/*PWM configuration*/


	/* Creates the blinker thread. */
	chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
	chprintf((BaseChannel *)&SD1, "ADC TEST STAMP IFLAB \n\r\n");

	int16_t pot1 = 0;
	int16_t pot2 = 0;

	while (TRUE) {

		//Channel 4
		pot = 0;
		ADC_EnableChannel(ADC, ADC_CHANNEL_4);
		nvicEnableVector(ADC_IRQn, CORTEX_PRIORITY_MASK(SAM3_ADC_PRIORITY)); /* Enable ADC interrupt */
		ADC_EnableIt(ADC,ADC_IER_RXBUFF); /* Enable PDC channel interrupt */
		ADC_StartConversion(ADC); /* Start conversion */
		while ( !conversionDone );
		if ( conversionDone ) conversionDone = 0;
		ADC_DisableChannel(ADC, ADC_CHANNEL_4);

		//Channel 5
		pot = 1;
		ADC_EnableChannel(ADC, ADC_CHANNEL_5);
		nvicEnableVector(ADC_IRQn, CORTEX_PRIORITY_MASK(SAM3_ADC_PRIORITY)); /* Enable ADC interrupt */
		ADC_EnableIt(ADC,ADC_IER_RXBUFF); /* Enable PDC channel interrupt */
		ADC_StartConversion(ADC); /* Start conversion */
		while ( !conversionDone );
		if ( conversionDone ) conversionDone = 0;
		ADC_DisableChannel(ADC, ADC_CHANNEL_5);

		pot1 = adc_values1[0]*99/1023;
		pot2 = adc_values2[0]*99/1023; 

		if (pot1<10 && pot2<10) chprintf((BaseChannel *)&SD1, "1-0%d-2-0%d   \r",pot1,pot2);
		else if (pot1<10) 	chprintf((BaseChannel *)&SD1, "1-0%d-2-%d   \r",pot1,pot2);
		else if (pot2<10) 	chprintf((BaseChannel *)&SD1, "1-%d-2-0%d   \r",pot1,pot2);
		else 			chprintf((BaseChannel *)&SD1, "1-%d-2-%d   \r",pot1,pot2);

		chThdSleepMilliseconds(500);
	}

	return(0);
}
