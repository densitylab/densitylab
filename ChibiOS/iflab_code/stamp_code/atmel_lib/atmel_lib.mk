# List of all the embryo library files
#		  ${ATMEL_SRC}/embryo_str.c \
#		  ${ATMEL_SRC}/embryo_time.c \
#		  ${ATMEL_SRC}/embrio.c		
#                 ${ATMEL_SRC}/efc.c \
#                 ${ATMEL_SRC}/flashd.c  


ATMEL_LIB_SRC	= ${ATMEL_SRC}/atmel_adc.c \
                  ${ATMEL_SRC}/atmel_pwm.c \
                  ${ATMEL_SRC}/atmel_pmc.c \
                  ${ATMEL_SRC}/atmel_pio.c \
                  ${ATMEL_SRC}/atmel_usart.c \
                  ${ATMEL_SRC}/atmel_twi.c \
                  ${ATMEL_SRC}/atmel_twid.c \
                  ${ATMEL_SRC}/atmel_pio_it.c \
                  ${ATMEL_SRC}/rtc_ds3231.c
# Required include directories
ATMEL_LIB_INC = ${ATMEL_SRC}/include
