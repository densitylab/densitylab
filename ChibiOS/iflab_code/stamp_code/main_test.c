#include "ch.h"
#include "hal.h"
#include "test.h"
#include "chprintf.h"


static WORKING_AREA(waThread1, 128);
static msg_t Thread1(void *arg) {
  (void)arg;
  while (TRUE) {
    palClearPad(IOPORT2, PIOB_LED2);
    chThdSleepMilliseconds(500);
    palSetPad(IOPORT2, PIOB_LED2);
    chThdSleepMilliseconds(500);
  }
  return(0);
}


void main (void)
 {
  
  halInit();
  chSysInit();
  sdStart(&SD1, NULL);  /* Activates the serial driver 2 */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  
  unsigned char data [10] = {10 , 11 , 12 , 13 , 11 , 12 , 13 , 11 , 12 , 13};
	while(1){
	
    	for(int i = 0 ; i < 10 ; i++){
    	    chprintf((BaseChannel *)&SD1,"%d;", data[i] );
    	}	
	
	}
 }



