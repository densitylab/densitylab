# List of all the embryo library files
#		  ${ATMEL_SRC}/embryo_str.c \
#		  ${ATMEL_SRC}/embryo_time.c \
#		  ${ATMEL_SRC}/embrio.c		


ATMEL_LIB_SRC	= ${ATMEL_SRC}/atmel_adc.c \
                  ${ATMEL_SRC}/atmel_pwm.c

# Required include directories
ATMEL_LIB_INC = ${ATMEL_SRC}/include
